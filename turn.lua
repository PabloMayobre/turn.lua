------------- SCREEN PART -------------
local screen={}
local _realscreen={}

_realscreen.w, _realscreen.h = love.window.getDimensions() --get the height and the width of the real screen (doesn't matter the orientation)
screen.w, screen.h = _realscreen.w, _realscreen.h --preset the screen height and width of the screen

local _deforient=(_realscreen.w > _realscreen.h and "landscape") or "portrait" --default orientation detection
local _add = (_deforient == "portrait" and 1) or 0 --this is the added value depending on the default orientation

--THIS FUNCTION RETURNS THE DEFAULT ORIENTATION IN THE FORMAT (string"orienation", boolean"upside down", number"turn")
screen.default = function()
	return _deforient,false,0
end

--IF YOU CHANGE THE SCREEN MODE BE SURE TO CALL THIS FUNCTION SINCE YOU MAY OVERRIDE SOME VALUES IF YOU DONT
screen.update = function()
	_realscreen.w, _realscreen.h = love.window.getDimensions()
	_deforient=(_realscreen.w > _realscreen.h and "landscape") or "portrait"
	_add = (_deforient == "portrait" and 1) or 0
end
--THIS FUNCTION SETS "screen.w" AND "screen.h" TO THE VALUES THAT WILL BE USED WHEN THE DRAW CALL IS DONE
screen.preturn = function(turn)
	local turn = turn % 2 --evens are equal to the default orientation
	if turn == 0 then --evens
		screen.w, screen.h = _realscreen.w, _realscreen.h --we are in the default mode
	else --odds
		screen.w, screen.h = _realscreen.h, _realscreen.w --we are not in the default mode
	end 
	return --variables set to the right value
end

--THIS FUNCTION TURNS THE SCREEN IN THE RIGHT ANGLE--
screen.turn = function(turn) 
	local turn = turn % 4 --we have just four possible orientations 
	screen.preturn(turn) --we make a preturn so that we can define the screen variables
	if turn == 0 then return end --we are at default so we don't care about the rest
	local _trans = {x = 0; y = 0} --define the translation table
	if turn < 3 then _trans.x = _realscreen.w else _trans.x = 0 end --the code that handles the translation
	if turn > 1 then _trans.y = _realscreen.h else _trans.y = 0 end --idem
	love.graphics.translate(_trans.x,_trans.y) --we now translate the origing to where it needs to be before rotating
	love.graphics.rotate(math.pi / 2 * turn) --we rotate 90 degrees for every turn
	return --done
end

------------- ORIENTATION PART -------------
local _joysticks = love.joystick.getJoysticks() --we store all the joysticks
for i, _joy in ipairs(_joysticks) do --we iterate through them
	if _joy:getName()=="Android Accelerometer" then --check if it is the one we need
		_accel=_joy --store it
		_joysticks=nil --DELETE ALL THE VARIABLES!!
		_joy=nil
		break --we are done
	end
end
--This could be done better (probably on my next revision)
local _miv={true,true,true} --this table stores if the minimum value was reached for each axis
local _mav={false,false,false} --the same but for max values
local _lastchanged={ --this stores the time of the last change
	{mi=0,ma=0},
	{mi=0,ma=0},
	{mi=0,ma=0}
}
local _mis={0,0,0} --the sign in when it changed
local _mas={0,0,0} --idem
--end awfaul part
local _state={}
_state.orn, _state.ud, _state.tur = screen.default() --just setting the state to default


local function _sign(n) return n>0 and 1 or n<0 and -1 or 0 end --a snippet that gives the sign of a number

local orientation = {}

--THIS FUNCTION RETURN THE ACCELEROMETER JOYSTICK (SO YOU CAN USE IT EASIER)
orientation.getAccel = function()
	return _accel --just return the accelerometer so that you can use it too
end

--THIS FUNCTION RETURNS IF IT IS POSSIBLE TO CHANGE THE ORIENTATION WITH THE ACCELEROMETER (ACCELEROMETER EXISTS)
orientation.isPossible = function()
	return _accel and true
end

--THIS FUNCTION RETURNS THE CURRENT ORIENTATION OF THE DEVICE
orientation.update = function(dt, orn, ud, tur)
	for i=1,_accel:getAxisCount()--[[3]] do --get the number of axis of the accelerometer (could be hardcoded to 3 thought)
		_lastchanged[i].mi=_lastchanged[i].mi+dt --add dt to the time of the last change
		_lastchanged[i].ma=_lastchanged[i].ma+dt --idem
		local _mi=(math.abs(_accel:getAxis(i)*100))<30 --check if we are under the minimum
		local _ma=(math.abs(_accel:getAxis(i)*100))>70 --check if we are past the maximum
		if not _mi == _miv[i] then --differes from the last state
			if _lastchanged[i].mi>0.3 then --the time that took to change is greater than 0.3 seconds
				_miv[i]=_mi --change the state
				_lastchanged[i].mi=0 --reset the time
				_mis[i]=_sign(_accel:getAxis(i)) --store the sign of the change
			end
		end
		if not _ma == _mav[i] then --differs from the last state
			if _lastchanged[i].ma>0.3 then --the time that took to change is greater than 0.3 seconds
				_mav[i]=_ma --change the state
				_lastchanged[i].ma=0 --reset the time
				_mas[i]=_sign(_accel:getAxis(i)) --store the sign of the change
			end
		end
		_mi,_ma = nil, nil
	end
	
	--THIS SECTION IS A LITTLE BUGGY BUT IT GIVES YOU A DEFAULT MODE WHEN THE PHONE IS LAYING FLAT (PS: At the end of the comment it says elseif, be sure to delete the extra if after uncommenting)
	
	--[[if miv[1] and miv[2] and miv[3] then
		_state={}
		_state.orn, _state.ud, _state.tur = screen.default()
	elseif]]
	if _miv[1] and _mav[2] and _mav[3] then --change if we are on landscale
		_state.orn="landscape" --set the state to landscape
		if _mas[2]==1 then --upside down?
			_state.tur=0+_add
			_state.ud=false --nope
		elseif _mas[2]==-1 then
			_state.tur=2+_add
			_state.ud=true --yes
		else
			_state.tur=0+_add
			_state.ud=false --not sure
		end
	elseif _mav[1] and _miv[2] and _mav[3] then --check if we are on portrait
		_state.orn="portrait" --set the state to portrait
		if _mas[1]==1 then --upside down?
			_state.tur=3 + _add
			_state.ud=false --nope
		elseif _mas[1]==-1 then
			_state.tur=1 + _add
			_state.ud=true --yes
		else
			_state.tur=3 + _add
			_state.ud=false --not sure
		end
	end
	return _state.orn, _state.ud, _state.tur --return the right values
end

--tab={screen=screen, orientation=orientation}
return function() return screen,orientation end --return a function that returns the two tables separately, so you can dump one if you dont need it 