love.load = function()
	local _, _, flags = love.window.getMode()
	local w, h = love.window.getDesktopDimensions(flags.display)
	local success = love.window.setMode( w, h, flags)
	screen,orientation = require ("turn")() --sorry about those parenthesis there, it was the only way
	orn,ud,tur = screen.default() --storing default values so that nothing breaks
	color = {0, 0, 255}
end
love.update = function(dt)
	if orientation.isPossible() then --check if the accelerometer is available
		orn,ud,tur = orientation.update(dt) --update the orientation and get the values
		if orn == "landscape" then
			text = "Landscape Mode"
			color[3]=150
			color[2]=0
		else
			text = "Portrait Mode"
			color[2]=150
			color[3]=0
		end
		if ud then
			color[1]=150
		else
			color[1]=0
		end
	else
		color = {255, 0, 0}
		text = "No Accelerometer"
	end
end
love.draw = function()
	screen.turn(tur) --turn the screen
	love.graphics.setBackgroundColor(color)
	love.graphics.printf(text, 5, (screen.h/2)-6,screen.w-10,"center")
end
love.keypressed = function (key)
	if key == "escape" then
		love.event.quit()
	end
end