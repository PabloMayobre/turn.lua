#Example 1

This examples displays the current orientation in the center of the screen.

The text should be in the same orientation as the phone and turn around as you turn it

The color should change depending on the orientation

If no accelerometer is find a "No Accelerometer" red screen is shown. 

Remember to include `turn.lua` in the main path, then run it with LÖVE/LÖVE-Android

You can grab this example from the [Downloads](https://bitbucket.org/PabloMayobre/turn.lua/downloads) section as .love file