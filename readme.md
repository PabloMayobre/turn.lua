#Turn.lua

Turn.lua is a library for LÖVE, made with Android in mind.

It helps defining the orientation the game is currently using and it can be used in a variety of ways

##Using this library

This is pretty easy you just need to `require` this library in your `love.load` function. Thought you have to do it this way

	screen, orientation = require "turn" ()

The parenthesis allow the module to return both tables for you. Now you are free to use it the way you want

##Two main functionalities

* The first one is the screen functionality that allows to change the orientation on the screen
* The second one is the orientation, this takes the accelerometer and reads its values to determine the orientation of the phone

###Firts functionality: Screen

Screen is what handles the rendering of the screen, this table has some very useful functions:

`screen.default()` This function returns: 

* orientation (string): A string containing the default orientation, "landscape" or "portrait"
* upside-down (boolean): This is false. Since the default orientation can't be upside down
* tur (number): This number is 0 and is the number you would pass to `screen.turn()`

`screen.update()` This function returns nothing, you should call it every time you change the window mode to ensure that everything will work as expected.

`screen.preturn(turn)` This function returns nothing, It changes `screen.h` and `screen.w` depending on the `turn` value you pass so that you can make some calculations with them before drawing to screen

`screen.turn(turn)` This function returns nothing, it rotates and translate the origing to fit the current orientation defined by `turn`.

`scree.h` and `screen.w` This is what you should use instead of `getDimensions` to know the height and width of the window

###Second functionality: Orientation

Orientation handles the accelerometer and detects the current orientation of the phone, the functions are:

`orientation.getAccel()` Returns the Joystick object that corresponds to the Android Accelerometer

`orientation.isPossible()` Returns true if an accelerometer exists and orientation calculations can be performed

`orientation.update(dt)` This function returns:

* orientation (string): A string containing the current orientation, "landscape" or "portrait"
* upside-down (boolean): This booleans tells you if the phone is upside down in the current orientation
* tur (number): This number is the value you would pass to `screen.turn()` so that the screen drawing operations match with the orientation of the phone

##License

This library is licensed under MIT license